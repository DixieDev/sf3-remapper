extends Control

@onready var container = get_node("Controllers");
@onready var p1_arrow = get_node("LArrow")
@onready var p2_arrow = get_node("RArrow")
@onready var p1_target = get_node("Controllers/P1 Target")
@onready var p2_target = get_node("Controllers/P2 Target")
@onready var ready_bar = get_node("Ready Bar")

var icon_scene = preload("res://scenes/controller_icon.tscn")
var binding_menu_scene = preload("res://scenes/binding_menu.tscn")

# The controllers assigned for p1 and p2. These are the IDs of the controllers,
# which are not necessarily the same as their indexes in the array.
var p1_assignment = null
var p2_assignment = null

# Where the controllers start spawning
var starting_x = 0
var starting_y = 0

# The number of currently recorded pads
var num_pads = 0
var controllers = []

# Deadzone for axis inputs
var deadzone = 0.5

# Called when the node enters the scene tree for the first time.
func _ready():
	Jukebox.play_bgm()
	var pads = Input.get_connected_joypads();
	num_pads = pads.size()
	starting_x = container.position.x + container.size.x
	starting_y = container.position.y + container.size.y / 2.0 - 96
	for pad in pads:
		add_icon(pad, starting_y + 64 * pad)
	
	Input.joy_connection_changed.connect(joy_connection_changed)

func add_icon(id, y_pos):
	var icon = icon_scene.instantiate()
	icon.position.y = y_pos
	container.add_child(icon)
	controllers.append({ "icon": icon, "id": id, "pressed": false })

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var new_pad_count = Input.get_connected_joypads().size()
	if new_pad_count > num_pads:
		for pad in range(num_pads, new_pad_count):
			add_icon(pad, starting_y * pad)
		num_pads = new_pad_count;
	
	p1_arrow.visible = p1_assignment == null
	p2_arrow.visible = p2_assignment == null
	
	handle_controller_inputs()
	lerp_icons(delta)
	
	ready_bar.p1 = p1_assignment
	ready_bar.p2 = p2_assignment

func lerp_icons(delta):
	for controller in controllers:
		if p1_assignment == controller.id:
			var target = p1_target.position
			target.x -= 32
			controller.icon.position += (target - controller.icon.position) * delta * 3.0
		elif p2_assignment == controller.id:
			var target = p2_target.position
			target.x -= 32
			controller.icon.position += (target - controller.icon.position) * delta * 3.0
		else:
			var origin = Vector2(10.0, starting_y + 64 * controller.id)
			controller.icon.position += (origin - controller.icon.position) * delta * 3.0


func handle_controller_inputs():
	for controller in controllers:
		var axis = Input.get_joy_axis(controller.id, JOY_AXIS_LEFT_X)
		
		# Check for D-Pad inputs and emulate an axis value if they're pressed
		if Input.is_joy_button_pressed(controller.id, JOY_BUTTON_DPAD_RIGHT):
			axis = 1.0
		elif Input.is_joy_button_pressed(controller.id, JOY_BUTTON_DPAD_LEFT):
			axis = -1.0
		
		# If a controller is marked as being pressed, check if it's unpressed 
		# and move onto the next loop iteration
		if controller.pressed:
			if axis < deadzone and axis > -deadzone:
				controller.pressed = false
			continue
		
		# If the axis is currently pushed far enough, flag the controller as 
		# being pressed
		if axis > deadzone or axis < -deadzone:
			controller.pressed = true
		
		# If the controller is currently assigned, then handle unassignments
		if p1_assignment == controller.id and axis > deadzone:
			p1_assignment = null
			Jukebox.play_select()
			continue
		if p2_assignment == controller.id and axis < -deadzone:
			p2_assignment = null
			Jukebox.play_select()
			continue
		
		# Otherwise we handle assigning to a particular player
		if axis < -deadzone and p1_assignment == null:
			p1_assignment = controller.id
			Jukebox.play_select()
		if axis > deadzone and p2_assignment == null:
			p2_assignment = controller.id
			Jukebox.play_select()

func joy_connection_changed(id, connected):
	if not connected:
		if id == p1_assignment:
			p1_assignment = null
		if id == p2_assignment:
			p2_assignment = null

func on_both_players_ready():
	var next = binding_menu_scene.instantiate()
	get_tree().root.add_child(next)
	next.p1 = p1_assignment
	next.p2 = p2_assignment
	get_tree().root.remove_child(self)
