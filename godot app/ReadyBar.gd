extends Control

signal both_players_ready

@onready var ready_sfx = get_node("Ready SFX")
@onready var p1_ready_icon = get_node("P1 Ready")
@onready var p2_ready_icon = get_node("P2 Ready")
var p1 = null
var p2 = null

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if p1 != null and p2 != null:
		visible = true
		var p1_ready = false
		var p2_ready = false
		
		for button in range(0, 10):
			if Input.is_joy_button_pressed(p1, button):
				p1_ready = true
			if Input.is_joy_button_pressed(p2, button):
				p2_ready = true
		
		p1_ready_icon.visible = p1_ready
		p2_ready_icon.visible = p2_ready
		
		if p1_ready and p2_ready:
			Jukebox.play_confirm()
			emit_signal("both_players_ready")
	else:
		visible = false
