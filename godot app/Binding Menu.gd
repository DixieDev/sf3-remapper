extends Control

var BUTTON_COIN = 0
var BUTTON_START = 1
var deadzone = 0.5
var message_list = [
	"Press your Coin button",
	"Press your Start button",
	"Press Up",
	"Press Down",
	"Press Left",
	"Press Right",
	"Press Light Punch",
	"Press Medium Punch",
	"Press Heavy Punch",
	"Press Light Kick",
	"Press Medium Kick",
	"Press Heavy Kick",
	"Press your macro for 3 punches",
	"Press your macro for 3 kicks"
]

@onready var ready_bar = get_node("ReadyBar")
@onready var instructions = [
	get_node("P1 Label/Instruction"),
	get_node("P2 Label/Instruction")
]

# Controller IDs for each player
var p1 = null
var p2 = null


var pressed = [true, true]

# Each entry is an array of player mappings. For example:
# `{ "type": "button", "id": 8 }` or `{ "type": "axis", "id": 1, "direction": "+" }`
var maps = [[], []]

# _ready is called before p1 and p2 are populated with valid data. Until they are, this should be
# set to true
var awaiting_players = true

# Called once p1 and p2 are filled with valid joystick IDs
func on_players_ready():
	awaiting_players = false
	print_debug("P1: ", p1, " Index: ", pad_index(p1))
	print_debug("P2: ", p2, " Index: ", pad_index(p2))

func pad_index(p):
	var info = Input.get_joy_info(p)
	if info.has("xinput_index"):
		return info.xinput_index
	return p



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	# Handle waiting for p1 and p2 to be filled
	if awaiting_players:
		if p1 != null and p2 != null:
			on_players_ready()
		else:
			return
	
	#handle_depress()
	
	handle_player_button_mapping(p1, 0)
	handle_player_button_mapping(p2, 1)
	handle_press_state()
	
	if maps[0].size() == message_list.size() and maps[1].size() == message_list.size():
		ready_bar.p1 = p1
		ready_bar.p2 = p2
	else:
		ready_bar.p1 = null
		ready_bar.p2 = null

func handle_player_button_mapping(joy_id, player_idx):
	if pressed[player_idx]:
		return
	
	var finished = false
	var map = maps[player_idx]
	if map.size() < message_list.size():
		instructions[player_idx].text = message_list[map.size()]
	else:
		instructions[player_idx].text = "Mapping complete.\nPress Coin if you want to rebind"
		finished = true
	
	var button = get_pressed_button(joy_id)
	if button != null:
		if map.size() > 0 and map[BUTTON_COIN].type == "button" and map[BUTTON_COIN].id == button:
			Jukebox.play_confirm()
			map.clear()
			return
		if not finished:
			if map.size() > 1 and map[BUTTON_START].type == "button" and map[BUTTON_START].id == button:
				Jukebox.play_skip()
				map.append({ "type": "unassigned", "id": null })
				return
		
			Jukebox.play_select()
			map.append({ "type": "button", "id": button})
			return
	
	var held_axis = get_held_axis(joy_id)
	if held_axis != null:
		var axis = Input.get_joy_axis(joy_id, held_axis)
		var direction = "+"
		if axis < 0:
			direction = "-"
		
		if map.size() > 0 and map[BUTTON_COIN].type == "axis":
			if map[BUTTON_COIN].id == held_axis and map[BUTTON_COIN].direction == direction:
				Jukebox.play_confirm()
				map.clear()
				return
		
		if not finished:
			if map.size() > 1 and map[BUTTON_START].type == "axis":
				if map[BUTTON_START].id == held_axis and map[BUTTON_START].direction == direction:
					Jukebox.play_skip()
					map.append({ "type": "unassigned", "id": null })
					return
		
			Jukebox.play_select()
			map.append({ "type": "axis", "id": held_axis, "direction": direction })
			return
	

# Unsets the p#_pressed flags if players controllers are completely neutral
func handle_depress():
	if pressed[0] and not is_anything_held(p1):
		pressed[0] = false
	if pressed[1] and not is_anything_held(p2):
		pressed[1] = false

# Records whether or not inputs are held by a controller this frame
func handle_press_state():
	pressed[0] = is_anything_held(p1)
	pressed[1] = is_anything_held(p2)

func is_anything_held(joy_id):
	if get_pressed_button(joy_id) != null:
		return true
	if get_held_axis(joy_id) != null:
		return true
	return false

func get_pressed_button(joy_id):
	for button in range(0, JOY_BUTTON_MAX):
		if Input.is_joy_button_pressed(joy_id, button):
			return button
	return null

func get_held_axis(joy_id):
	for axis in range(0, JOY_AXIS_MAX):
		var input = Input.get_joy_axis(joy_id, axis)
		if abs(input) > deadzone:
			return axis
	return null


func on_both_players_ready():
	print_debug("Both players ready, saving mappings to file")
	
	# Write mappings to file
	var p1_data = {
		"joy_id": pad_index(p1),
		"map": maps[0]
	}
	var p1_map = FileAccess.open("user://p1_map.sav", FileAccess.WRITE)
	p1_map.store_line(JSON.stringify(p1_data))
	p1_map.close()
	
	var p2_data = {
		"joy_id": pad_index(p2),
		"map": maps[1]
	}
	var p2_map = FileAccess.open("user://p2_map.sav", FileAccess.WRITE)
	p2_map.store_line(JSON.stringify(p2_data))
	p2_map.close()
	
	Jukebox.stop_bgm()
	
	var exe_dir = OS.get_executable_path()
	var last_slash = exe_dir.rfind("/")
	exe_dir = exe_dir.substr(0, last_slash)
	OS.execute(exe_dir + "/sf3-mapping-replacer.exe", [])
	
	get_tree().change_scene_to_file("res://scenes/player_assignment.tscn")
	get_tree().root.remove_child(self)
