extends TextureRect

var bg_paths = [
	"res://stages/akuma.png",
	"res://stages/alex.png",
	"res://stages/chun.png",
	"res://stages/elena.png",
	"res://stages/ibuki.png",
	"res://stages/ken.png",
	"res://stages/necro.png",
	"res://stages/oro.png",
	"res://stages/twelve.png",
	"res://stages/urien.png"
]

# Called when the node enters the scene tree for the first time.
func _ready():
	# Pick a random texture and use it
	var idx = randi() % bg_paths.size()
	texture = load(bg_paths[idx])
	print_debug("Loaded bg ", bg_paths[idx])
