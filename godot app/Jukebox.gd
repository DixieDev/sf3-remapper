extends Control

@onready var select = get_node("Select SFX")
@onready var confirm = get_node("Confirm SFX")
@onready var skip = get_node("Skip SFX")
@onready var bgm = get_node("BGM")

func play_select():
	select.play()

func play_skip():
	skip.play()

func play_confirm():
	confirm.play()

func play_bgm():
	bgm.play()

func stop_bgm():
	bgm.stop()

func _on_bgm_finished():
	bgm.play()
