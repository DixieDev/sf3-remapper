use std::path::PathBuf;

#[derive(serde::Deserialize)]
struct Config {
  fightcade_path: String,
}

#[derive(serde::Deserialize)]
struct PlayerData {
  joy_id: u64,
  map: Vec<Mapping>,
}

#[derive(serde::Deserialize)]
#[serde(tag = "type")]
enum Mapping {
  #[serde(rename = "button")]
  Button {
    id: u64,
  },

  #[serde(rename = "axis")]
  Axis {
    id: u64,
    direction: String,
  },

  #[serde(rename = "unassigned")]
  Unassigned {
    id: Option<u64>,
  },
}
/*
	"Press your Coin button",
	"Press your Start button",
	"Press Up",
	"Press Down",
	"Press Left",
	"Press Right",
	"Press Light Punch",
	"Press Medium Punch",
	"Press Heavy Punch",
	"Press Light Kick",
	"Press Medium Kick",
	"Press Heavy Kick",
	"Press your macro for 3 punches",
	"Press your macro for 3 kicksj"*
  */

static DPAD_MAPPING: &[(u64, u64)] = &[
  (11, 12), // UP
  (12, 13), // DOWN
  (13, 10), // LEFT
  (14, 11), // RIGHT
];

static BUTTON_MAPPING: &[(u64, u64)] = &[
  // ABXY are all normal
  (0, 0),
  (1, 1),
  (2, 2),
  (3, 3),

  // Bumpers are whack
  (9, 4),
  (10, 5),

  // Start & Select are whack
  (4, 6),
  (6, 7),

  // Stick buttons are whack
  (7, 8),
  (8, 9),
];

static BUTTON_SET_1: &[&str] = &[
  "Coin",
  "Start",
  "Up",
  "Down",
  "Left",
  "Right",
  "Weak Punch",
  "Medium Punch",
  "Strong Punch",
  "Weak Kick",
  "Medium Kick",
  "Strong Kick",
];

fn main() {
  let config_file = {
    let mut p = std::env::current_exe().unwrap();
    p.pop();
    p.join("config.json")
  };
  println!("{:?}", config_file);
  let config_data = std::fs::read_to_string(config_file).unwrap();
  let config: Config = serde_json::from_str(&config_data).unwrap();

  // Path to ini file that we'll eventually write to
  let ini_path = PathBuf::from(&config.fightcade_path)
    .join("emulator/fbneo/config/games/sfiii3nr1.ini");

  let app_data = dirs::config_dir().unwrap();
  let gd_data = app_data.join("Godot/app_userdata/SF3 Input Mapper");
  let p1_map_file = std::fs::read_to_string(gd_data.join("p1_map.sav")).unwrap();
  let p2_map_file = std::fs::read_to_string(gd_data.join("p2_map.sav")).unwrap();

  let p1: PlayerData = serde_json::from_str(&p1_map_file).unwrap();
  let p2: PlayerData = serde_json::from_str(&p2_map_file).unwrap();


  let p1_prefix = format!("{}", 40 + p1.joy_id);
  let p2_prefix = format!("{}", 40 + p2.joy_id);

  let p1_switches = gen_player_switches(&p1_prefix, &p1.map);
  let p2_switches = gen_player_switches(&p2_prefix, &p2.map);

  let mut output = "
version 0x029744
analog  0x0100
cpu     0x0100

".to_string();

  for (idx, button) in BUTTON_SET_1.iter().enumerate() {
    let switch = &p1_switches[idx];
    output.push_str(&format!("input \"P1 {button}\" switch {switch}\n"));
  }

  for (idx, button) in BUTTON_SET_1.iter().enumerate() {
    let switch = &p2_switches[idx];
    output.push_str(&format!("input \"P2 {button}\" switch {switch}\n"));
  }

  output.push_str("
input \"Reset\" switch 0x3D
input \"Diagnostic\" switch 0x3C
input \"Service\" switch 0x0A
input \"Region\" constant 0x01
input \"Fake Dip\" constant 0x00

macro  \"System Pause\"     undefined
macro  \"System FFWD\"      undefined
macro  \"System Frame\"     undefined
macro  \"System Load State\" undefined
macro  \"System Save State\" undefined
macro  \"System UNDO State\" undefined
macro  \"Lua Hotkey 1\"     undefined
macro  \"Lua Hotkey 2\"     undefined
macro  \"Lua Hotkey 3\"     undefined
macro  \"Lua Hotkey 4\"     undefined
macro  \"Lua Hotkey 5\"     undefined
macro  \"Lua Hotkey 6\"     undefined
macro  \"Lua Hotkey 7\"     undefined
macro  \"Lua Hotkey 8\"     undefined
macro  \"Lua Hotkey 9\"     undefined
macro  \"P1 Auto-Fire Button 1\" undefined
macro  \"P1 Auto-Fire Button 2\" undefined
macro  \"P1 Auto-Fire Button 3\" undefined
macro  \"P1 Auto-Fire Button 4\" undefined
macro  \"P1 Auto-Fire Button 5\" undefined
macro  \"P1 Auto-Fire Button 6\" undefined
macro  \"P2 Auto-Fire Button 1\" undefined
macro  \"P2 Auto-Fire Button 2\" undefined
macro  \"P2 Auto-Fire Button 3\" undefined
macro  \"P2 Auto-Fire Button 4\" undefined
macro  \"P2 Auto-Fire Button 5\" undefined
macro  \"P2 Auto-Fire Button 6\" undefined
");

  output.push_str(&format!("macro \"P1 3x Punch\" switch {}\n", p1_switches[12]));
  output.push_str(&format!("macro \"P1 3x Kick\" switch {}\n", p1_switches[13]));
  output.push_str(&format!("macro \"P2 3x Punch\" switch {}\n", p2_switches[12]));
  output.push_str(&format!("macro \"P2 3x Kick\" switch {}\n", p2_switches[13]));

  std::fs::write(ini_path, output.as_bytes()).unwrap();


  let fbneo_path = PathBuf::from(&config.fightcade_path).join("emulator/fbneo/fcadefbneo.exe");
  
  std::process::Command::new(fbneo_path.into_os_string().into_string().unwrap())
    .arg("sfiii3nr1")
    .spawn()
    .unwrap();
}

fn gen_player_switches(prefix: &str, map: &[Mapping]) -> Vec<String> {
  let mut output = Vec::new();
  for mapping in map.iter() {
    match mapping {
      Mapping::Button { id } => {
        if let Some((_, fc_id)) = DPAD_MAPPING.iter().find(|(gd_id, _)| *gd_id == *id) {
          output.push(format!("0x{prefix}{fc_id}"));
          continue;
        }

        if let Some((_, fc_id)) = BUTTON_MAPPING.iter().find(|(gd_id, _)| *gd_id == *id) {
          output.push(format!("0x{prefix}{}", 80 + fc_id));
        }
      },

      Mapping::Axis { id, direction} => {
        let fc_id = match (id, direction.as_str()) {
          // X Axis
          (0, "-") => "00",
          (0, "+") => "01",

          // Y Axis
          (1, "-") => "02",
          (1, "+") => "03",

          // Z Axis (triggers)
          (5, "+") => "04",
          (4, "+") => "05",

          // Right-stick X Axis
          (2, "-") => "06",
          (2, "+") => "07",

          // Right-stick Y Axis
          (3, "-") => "08",
          (3, "+") => "09",

          _ => panic!("WTF"),
        };

        output.push(format!("0x{prefix}{fc_id}"));
      }

      Mapping::Unassigned { .. } => {
        output.push("undefined".to_string());
      }
    }
  }

  output
}
