# SF3 Remapper

A tool for running Third Strike at locals via Fightcade's FBNeo distribution 
with minimal mouse or keyboard input involved. 

## Usage

This assumes you have both Fightcade and the Third Strike ROM set up already.

Make a file called `config.json` inside the same directory as `sf3-remap.exe`
and fill it with something like this:

```json
{
  "fightcade_path": "C:\\Path\\To\\Your\\Fightcade"
}
```

The path should lead to the directory you store Fightcade, the same directory
that contains `Fightcade2.exe` and the `emulator` directory. Remember to use 
2 backslashes instead of just 1.

Once this is done you can run the `sf3-remap.exe` program, plug in some 
controllers, and have a good time.


## Building 

Building requires Godot and Rust, and this only supports Windows. 

First, export the Godot project to a new directory. Then, compile the Rust 
code. This should be as easy as running `cargo build --release` in the 
`rust/sf3-mapping-replacer` directory. Copy the built executable into the 
same directory as the exported Godot app.

